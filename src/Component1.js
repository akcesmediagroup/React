import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Student from "./Student"
import {Button, Form, Input, Grid } from 'semantic-ui-react';

class Component1 extends Component {
  ComponentWillReceiveProps(new_props){
    console.log("Je Vais récupérer les props", new_props)
  }

  state = {
    current_student: {
      name:"",
      age:0,
    },
      height: 10,
      students: [] // {name: String, age: Number}
  }

  handleChangeStudent = (attr, event) => {
    const {current_student} = this.state
    current_student[attr] = event.target.value
    this.setState({current_student})
  }

  handleChange = (attr, e) => {
    let state = this.state
    state[attr] = e.target.value
    this.setState(state)
  }

  addStudent = (event) => {
    event.preventDefault()
    let {students, current_student} = this.state
    students.push(current_student)
    this.setState({
      students,
      current_student: {name: "", age: ""}
    })
  }

  removeStudent = (index) => {
    let {students} = this.state
    students.splice(index, 1)
    this.setState({
      students
    })
  }

  displayConsole = () =>{
    alert('ok');
  }

  render() {
    const { current_student, students } = this.state

    return (
      <Grid stackable>
      <Grid.Column width={16}>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">TITRE</h1>
        </header>
      </Grid.Column>
        <Grid.Column width={8}>
        <Input
         placeholder="Nom édudiant"
         onChange={ (e) => this.handleChangeStudent('name', e)}
         type="text"
         value={current_student.name}
         />
         </Grid.Column>
         <Grid.Column width={8}>
         <Input
          placeholder="age"
          onChange={ (e) => this.handleChangeStudent('age', e)}
          type="number"
          value={current_student.age}
          />
           </Grid.Column>
         {current_student.name && current_student.age &&  <Button color="blue" onClick={ this.addStudent }>Ajouter Etudiant</Button>}
        <Input value={this.state.height} onChange={(e) => this.handleChange('height', e)} type="number"/>
         { students.map((stu, index) => {
           return (
            <Student
            index={index}
            onRemove={this.removeStudent}
            student={stu}
            key={index + stu.name }
            pop={this.displayConsole}
            surname="jean jacques"
            heightWanted={this.state.height} />
           )
         })}
      </Grid>

    );
  }
}

export default Component1;
