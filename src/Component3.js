import React, { Component } from 'react';
import './App.css';
import {Grid, Input} from 'semantic-ui-react';

class Component3 extends Component {

  state = {
    navbar_height: "50",
    column_one_with:"8",
    column_two_with:"8",
  }
  handleChange = (e, {name, value}) => {
    let state = this.state
    state[name] = value
    this.setState(state)
  }


  render(){
    const {navbar_height, column_one_with, column_two_with, footer_height}=this.state

      return(
        <Grid stackable>
          <Grid.Column width={16} style={{height: navbar_height + "px"}} >
            <Input type='text' label="hauteur navbar" value={navbar_height} name="navbar_height" onChange={this.handleChange} />
            <Input type='text' label="colonne 1" value={column_one_with} name="column_one_with" onChange={this.handleChange} />
            <Input type='text' label="colone 2" value={column_two_with} name="column_two_with" onChange={this.handleChange} />
            <Input type='text' label="hauteur footer" value={navbar_height} name="footer_height" onChange={this.handleChange} />
          </Grid.Column>
          <Grid.Column width={16} style={{height: navbar_height}} >
            <p> navbar </p>
          </Grid.Column>
          <Grid.Column color="blue" width={column_one_with}>
            <p>blue</p>
          </Grid.Column>
          <Grid.Column color="red" width={column_two_with}>
            <p>red</p>
          </Grid.Column>
          <Grid.Column color="green" width={16}>
            <p>footer</p>
          </Grid.Column>
      </Grid>
      )
  }


  }

  export default Component3;
