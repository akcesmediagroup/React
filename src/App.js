import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Student from "./Student"
import Component1 from "./Component1"
import Component2 from "./Component2"
import Component3 from "./Component3"
import Product from "./Product"
import {Button, Form, Input, Grid } from 'semantic-ui-react';

export default class App extends Component {

  state = {
    cars:[
    {
      name: 'clio',
      description: "la voiture la plus rapide",
      image_url: '/img/clio.jpg',
    },
    {
      name: 'audi',
      description: "la voiture la plus rapide",
      image_url: 'https://eu.cdn.autosonshow.tv/1630/turntable/DZ957VH/01_md.jpg',
    }]
  }

  buyCar=(name, index)=>{
    let {cars} = this.state;
    cars[index].buy = true;
    this.setState({cars});
  }
  render() {
  const {cars} = this.state
    return (
      <div>
      { /* <Component1 />*/ }
      {/*<Component2/>*/}
      {/*<Component3/>*/}
      {cars.map((car,index)=> {
          return <Product product={car} index={index} key={index} onBuyClick={this.buyCar}/>
      })}

      </div>
    );
  }
}
