import React, { Component } from 'react';
import './App.css';

class Component2 extends Component {

  state = {
    counter: 0
  }

  increment = ()=>{
    this.setState({
      counter:this.state.counter + 1
    });
  }


  render(){
    const {counter}=this.state

      return(
        <div style={{textAlign:'center'}}>
        <h3>Compteur : {counter}</h3>
        <button onClick={this.increment}>+1</button>
        </div>
      )
  }


  }

  export default Component2;
