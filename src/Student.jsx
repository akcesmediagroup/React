import React, {Component} from 'react';
import {Button} from 'semantic-ui-react';

export default class Student extends Component{

  remove =()=>{
    console.log('appel de la remove student', this.props.student.name)
    this.props.onRemove(this.props.index)
  }

  popup = ()=>{
    this.props.pop()
  }


  render(){
      const {student} = this.props
      return (
        <div style={{height: this.props.heightWanted + 'px', backgroundColor: 'red'}}><p>{student.name}-{student.age}</p>
        <Button onClick={this.remove} content='Supprimer'/>
        <Button onClick={this.popup} content='wizz'/>
        </div>
      )
  }
}
