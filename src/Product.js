import React, { Component } from 'react';
import './App.css';
import {Card, Button, Image} from 'semantic-ui-react';

export default class Product extends Component {

  state = {

  }

  buy =()=>{
    this.props.onBuyClick(this.props.product.name, this.props.index)
  }


  render(){
      const {product}=this.props

      return(
        <Card>
     <Image src={this.props.product.image_url} />
     <Card.Content>
       <Card.Header>
        {this.props.product.name}
       </Card.Header>
       <Card.Description>
         {this.props.product.description}
       </Card.Description>
     </Card.Content>
     <Card.Content extra>
       <Button disabled={product.buy} onClick={this.buy}>Acheter </Button>
     </Card.Content>
   </Card>
      )
  }


  }
